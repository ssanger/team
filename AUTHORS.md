# Team

## 2020-2022

[Billy Horn🚀](https://gitlab.com/billy-horn)
[Constance Mlagami](https://gitlab.com/mlagamic)
[Copeland Royall](https://gitlab.com/coayer)
[Dwayne Negron](https://gitlab.com/dwayne.negron1)
[Greg Thompson](https://gitlab.com/thompsgj)
[Hanna Seyoum](https://gitlab.com/hmseyoum)
[Hobson Lane](https://gitlab.com/hobs)
[Jess C.](https://gitlab.com/cassjes.m)
[John May](https://gitlab.com/jmayjr)
[Jon Sundin](https://gitlab.com/jonsundin)
[Kalika Kay Curry](https://gitlab.com/KalikaKay)
[Kevin Sia](https://gitlab.com/kevin.sia)
[Makeda Gebremedhin](https://gitlab.com/makedag)
[Man Wai Yeung🏓](https://gitlab.com/dcvionwinnie)
[Maria Dyshel💬](https://gitlab.com/maria_tangibleai)
[Martha Gavidia](https://gitlab.com/marsgav)
[Maya Hecht](https://gitlab.com/MayaHe)
[Meijke](https://gitlab.com/meiji163)
[Uzo Ikwuakor](https://gitlab.com/Ikwuakor)
[Vishvesh Bhat](https://gitlab.com/vbhat1)
[Wamani Jacob🐒](https://gitlab.com/nicobwan)
[Winston](https://gitlab.com/winstonoakley)
[kazumaendo](https://gitlab.com/kazumaendo)
[Rochdi Khalid👽](https://gitlab.com/rochdikhalid)

## 2023

Cetin Cakir: @cetincakirtr
Chukwudi Ibe: @nitrospawn
Daniel Lantsevich: @digitalbraining
Greg Thompson: [@thompsgj](gitlab.com/thompsgj)
Hobson Lane: [@hobs](gitlab.com/hobs)
John May: [@jmayjr](https://gitlab.com/jmayjr)
Maria Dyshel: [@maria_tangibleai](gitlab.com/maria_tangibleai)
Rochdi Khalid @rochdikhalid
Rumman Rahib @RummanRahib
Sarah Sanger: [@ssanger](gitlab.com/ssanger) 
Sebastian Larson @catasaurus
Vlad Snisar: [@sviddo](gitlab.com/sviddo)
Maria Dyshel @maria_tangibleai

## References

[Black Hat Python](https://www.dropbox.com/s/98zlcm7axe3vhlm/BlackHatPython2E.pdf) by Justin Seitz and Tim Arnold 
[Django Tutorial nvidious videos](https://yewtu.be/playlist?list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p) by Corey Schafer
