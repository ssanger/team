# Conversation Design Datasets

## Context

- message history
- user profile
- slots/variables previously filled
- datetime

## Assessment/Quizzing

- question
- scoring function
- name of scoring attribute

## Slot filling

- utterance template (variables in `{curly_braces}`: `name = 'What is your name?'`
- bot message f-string templates `f'Hi {name}'`

## FAQ

## Intent

An intent recognition dataset requires at least two columns or fields:

- user utterance
- bot intent identifier

A third field for "context" is also sometimes helpful.

 "context", just as a hint about what's to come. Your content copywriters and conversation designers do not need to worry about context until we have defined state names and identifiers for the different locations in the dialog (introduction, microlesson, quiz, rapport-building, joke-telling) and student attributes (returning-student, new-student, advanced-student, graduate, pronouns-he-him-his, pronouns-they-them-theirs) that we want to use in Rori's decisionmaking.

## Bugs

- is
- should be
- context
