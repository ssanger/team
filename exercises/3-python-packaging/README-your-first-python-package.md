# Your First Python Package

The only required file for your package is a `pyproject.toml` file which specifies all your dependencies.
Poetry can create it for you.
And you'll want to do all your work in a virtualenv.

```bash
pip install poetry virtualenv
poetry init
```

You can manually edit the dependencies lists.
Here's an [example `pyproject.toml`](https://gitlab.com/tangibleai/community/mathtext/-/blob/main/pyproject.toml)

Your build.sh script can be used to install all your packages dependencies and install your package itself.

#### `build.sh`
```bash
deactivate
python3.9 -m pip install virtualenv
rm -rf .venv
python3.9 -m virtualenv --python 3.9 .venv
source ./.venv/bin/activate
pip install --upgrade -e .
```

## Ubuntu packages

```bash
sudo apt-get install software-properties-common
sudo update-alternatives --remove-all python
sudo apt install -y python3.7 python3.7-virtualenv python3.7-distutils python3.8 python3.8-virtualenv python3.8-distutils python3.9 python3.9-virtualenv python3.9-distutils
```

## Resources

- https://conf.sciwork.dev/2020/tutorial/packaging.html
- [Python Packaging User Guide](https://packaging.python.org/)
- [Conda-build documentation](https://docs.conda.io/projects/conda-build/)
- [open source license identifiers](https://spdx.org/licenses/)
- [pyproject.toml syntax](https://python-poetry.org/docs/pyproject/)
- [install pip](https://pip.pypa.io/en/stable/installation/)
