# Choose your own text adventure

Have you ever played a text adventure or role playing game?
In role playing, your friend acts as as the Dungeon Master. 
They describe a world to you and your "party" and you all decide what to do in the game.
In a text adventure a computer program is the Dungeon Master.
And the computer describes the world with text rather than voice (and hand waving ;).

#### IMPORTANT
Before you get started, make sure you've enrolled in the interactive textbook course with the name "sdccd_mesa_college_cs179_spring23". If Runestone is set up correctly, you should be able to see the _Foundations of Python Programming_ Table of Contents here: [runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/)


## Colossal Cave Adventure

To help get you started, here's the opening scene in the very first text adventure ever created:

```text
WELCOME TO ADVENTURE!!  WOULD YOU LIKE INSTRUCTIONS?
```

*`no`*

```text
YOU ARE STANDING AT THE END OF A ROAD BEFORE A SMALL BRICK BUILDING. AROUND YOU IS A FOREST. A SMALL STREAM FLOWS OUT OF THE BUILDING AND DOWN A GULLY.
YOU HAVE WALKED UP A HILL, STILL IN THE FOREST THE ROAD NOW SLOPES BACK DOWN THE OTHER SIDE OF THE HILL. THERE IS A BUILDING IN THE DISTANCE.
```

*`east`*

```text
YOU ARE INSIDE A BUILDING, A WELL HOUSE FOR A LARGE SPRING.

THERE ARE SOME KEYS ON THE GROUND HERE.

HERE IS A SHINY BRASS LAMP NEARBY.

THERE IS FOOD HERE.
```

### . . .


## **YOUR** Adventure

A lot of great programmers (like Willie Crowthers) got their start by building text adventures.
**Your mission, if you chose to accept it, is to create a text adventure.**
Your Python program only needs to be able to print things to the terminal screen and take input from the user.

The fun part is creating the rooms and levels of the game - the *text* in your text adventure.
But if this assignment is too fun for you, and you'd rather build something more serious, think about how your program could be a virtual assistant.
Text-based programs like this are the brains behind popular virtual assistants like [MyCroft.AI](https://MyCroft.AI), You.com, and [Qary.ai](https://qary.ai).
So feel free to turn your game into a "virtual assistant" or a simple chatbot.

The only requirements for this exercise are that your program:

* has 5 or more "rooms" (_states_)
* understands 4 or more commands (_keywords_)
* allows the user to visit all rooms
* allows the user to reach the end - your program should eventually _halt_ or exit, but **NOT** [HCF](https://en.wikipedia.org/wiki/Halt_and_Catch_Fire_(computing)) `;-)`

### Programming Roadmap

For every programming project you'll want to do these ten things:

1. _Ideation_ - think of an idea for your program
2. Sketch a _wireframe_ - think about what your program looks like when it runs
3. Write _pseudo code_ for a small piece of the program
4. Write Python code for a small piece of the program
5. Test the program yourself (_developer testing_)
6. Go to step 4 to *fix bugs* until your program works
7. Go to step 3 to *add a feature* that makes your program a little better... until the program is minimally useful
8. Release or share your program

### 1. Ideation

Dream up a story you'd like to tell with your game.
Is the player playing the role of a human, animal, monster, fantasy character, supernatural being, or alien?
Is the world present day Earth, a future dystopia, or "A long time ago in a galaxy far far away."
Feel free to sketch your ideas or draw mind maps, talk it over with friends or your fellow students.
The best ideas usually come out of the "collective consciousness" of several people working together.

The opening line is really important.
It's like the "pitch" for a Silicon Valley software startup.
How will your adventure start?
And how will your adventure end?
Keep your story short, for now, you can expand it later, once you have something built.
Maybe your player just needs to find their way out of a haunted house, or room, like a "Scooby Do" cartoon, or IRL Escape Room, a James Bond prison cell, or a Star Wars garbage compactor.

The hardest part is thinking up a **name** for your program.
You can change it later, but make sure it's short and sweet so your future self can easily type it to find it on your computer.
Create a directory on your computer to hold your program.

I keep all my programs in a directory called `code` in my user directory. So on my computer my "Cloak and Dagger" adventure might be in a path like this:

```bash
/home/hobs/code/spy_adventure/
```

### 2. Wireframe

Sketch out your idea on paper or in a drawing program or text editor on your computer. I like to use a markdown file called (`ideas.md`) in the `docs/` directory of the folder where I'm sorting on my project
So to build your text adventure you'll start with a "wire frame" or design so you can plan out your story.

Just like in life, it's easier for the machine to talk than it is to listen.
The hard part of building a text adventure is dealing with the text that the user ``input``s into the console of your game.

I keep all my documentation and notes in a directory called `docs` in my project. So my ideas would be in a file path like this:

```bash
/home/hobs/code/spy_adventure/docs/ideas.md
```

### 3. Pseudocode

Next you need to think about what your program needs to do.
You can think of pseudo code as an English recipe or set of instructions you would tell a human for how to do what you want them to do.
Only this human is trapped in a computer and can only manipulate data from input.
And it can only display text or graphics on the screen of the computer.
Pseudocode helps you plan your program.
You can see examples in Chapter 1, [section 1.2 in the FOPP textbook](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/GeneralIntro/Algorithms.html).

So open your `docs/ideas.md` file and add some pseudocode to help you plan the algorithms or functions you need for your text adventure game.

* What are the inputs to your program?
* What are the outputs to your program?
* What are the things the computer needs to do to the inputs to decide what to output?
* Can you think of some functions that might be useful in helping your program create that output?

Open your ideation notes and wireframe description file in your IDE so you can add some pseudocode to it. Smart IDEs like `Sublime Text` let you find files with <kbd>Ctrl</kbd> + <kbd>P</kbd>.
So tomorrow type <kbd>Ctrl</kbd> + <kbd>P</kbd> and then type "spyideasmd" and your IDE will go straight to your new game ideas file if it has those words in it.
And in even the most basic IDE or text editor, you can usually use <kbd>Ctrl</kbd> + <kbd>F</kbd> or <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>F</kbd> to find words within any file.
Here's your cheat sheet for finding file paths in your IDE:

* [Sublime Text](https://sublime-text-unofficial-documentation.readthedocs.io/en/sublime-text-2/reference/keyboard_shortcuts_win.html): <kbd>Ctrl</kbd> + <kbd>P</kbd>
* [PyCharm](https://www.jetbrains.com/help/pycharm/mastering-keyboard-shortcuts.html): <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>N</kbd>
* [Idle](https://defkey.com/python-idle-for-windows-shortcuts): <kbd>Ctrl</kbd> + <kbd>O</kbd> to open a file, it's not possible to search for filenames
* [VSCodium](https://github.com/VSCodium/vscodium): <kbd>Ctrl</kbd> + <kbd>P</kbd>

### 4. Python!!

Finally you can start writing some Python!
Create a new file called `app.py` in your project directory.
My full path looked like this: `/home/hobs/code/spy_adventure/app.py`.

Next you want to add some documentation or comments to the file.
Having this documentation is a great way to "pick up where you left off" each time you open the file.
Once you start to "think in Python," you will discover that your ideas (step 1), wireframe (step 2), and pseudocode (step 3) will contain bits of Python code that you can copy and paste into your Python file.
For now just paste some of your pseudocode as-is into a [docstring](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Functions/FunctionDefinitions.html?highlight=docstring) or [comment]() at the top of your `app.py` file.
Use the keyword `pass` for now in all your function definitions.

Create a main function called `app` in this file.
Make this a _stub_ that doesn't do anything useful.
You can use the `pass` keyword to create _stubs_ in Python:

```python
def do_something():
    pass
```

Or if you prefer you can create a _stub_ function by just returning `None`:

```python
def your_awesome_python_function():
    return None
```

Your main `app()` function will be used to call several other functions in your program.
All programs should be broken up into functions so that they are easy to debug and think about.
So go ahead and create some _stub_ functions for the algorithms or functions you dreamed up during ideation.
I can think of at least three useful functions for a text adventure program:

1. **input:** prompt the user for input and return that input
2. **game logic:** decide which action the user wants to perform and 
3. **output:** display the text describing the _state_ or "room" of your user's adventure

Now, within your `app()` function call all your stub functions in the order you want them to run.
They don't need to do anything, you just want to get the structure of your code working.

### 5. Test

The key to _active learning_ for anything you want to do in life is to

1. try stuff that may not work (make mistakes)
2. look for unexpected things that happen 
3. learn from your "mistakes" 

If you don't make mistakes, you don't have anything to learn.

So you need to test your application to see if there are mistakes.
This is how you can run your Python code:

```bash
python app.py
```

If you see error messages or unexpected output you will need to start debbugging it reread your code and see if you can find where it went wrong.

If don't your code does what you intended then it is _correct_ then your tests have "passed".
Whenever your code works the way you expect, you can skip step 6. ("Debug") and move on to step 7 (adding a feature).

Once you've created a working game, copy the text from your test session in the terminal (console).
Paste your game session into the discussion channel so your classmates can see.
Make sure you interact with your game for at least 4 inputs (commands) so you can get full credit.

### 6. Debug

Debugging code is an important skill.
You will get better and better at it over time.
FOPP has an [entire chapter](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/Debugging/toctree.html?highlight=debbug)

#### HINT:
There's another way to run Python programs that can sometimes be more useful when you are debugging.
Within the **i**Python interactive console (sometimes called a "REPL") you can use the magic command `%run` to process your file with the `python` interpretter.
First you need to launch the `ipython` interactive console:

```bash
ipython
```

Within the ipython console you

```bash
# In [1]
%run app.py
```

### 7. Add a feature

#### Output

You can start by creating a data structure to hold your room or scene descriptions.
If you can't think of a cool fantasy or SciFi world, you can use your home.
The goal is to get familiar with the mechanics of creating a game.
Your game just has to be fun enough that you don't get bored while testing it.

#### Input

You might want to have a list of four or five commands or actions that your players can use no matter where they are.
The commands "north", "south", "east", "west" are good ones if you see your world as a rectangular grid in your mind.
If your game is all about conversation and negotiation you might want to think about commands such as: say "hello", ask "question", tell "story", tell "joke", share "information", give "advice", say "goodbye".
If your game is about fighting you can add commands for "attack", "defend", "run" away, "duck", "hide."
Keep the list of commands small at first.
Until you learn about data-driven programs, you will need an `if` or `elif` statement for each command in each "room."
So if you have 5 rooms and 5 commands, that's 25 _conditional expressions_:

```python
command = command.strip().lower()
room = room.strip().lower()
if command[0] == 'n' and room == 'shed':
    # call function for room to the north of the shed
elif command[0] == 'e' and room == 'shed':
    # call function for room to the East of the shed
elif command[0] == 'n' and room == 'garden':
    # call function for room to the North of the garden
elif ...
else ...
``` 

A nice feature is if your commands each have different first letters so they will be easier to recognize, even if your user only types one letter.

You can also have a world where you move from place to place within a conversation, like a chatbot.

#### _Hint_
_Don't ever name any of your functions or variables `input`. You should never use any builtin Python keyword to name any objects (variables) ._


8. Share you program with the world!

For this exercise you can share your program with the rest of your class by posting a link to it in 
* Give your program to a friend and ask them what they think (_user testing_)... go back to step 3 to add your
9. Share your program by making it public on GitLab or a web page or forum (_software release_)
10. Profit `;-)` ... or get an A on your assignment
Give your program to someone else and ask them what they think (_user testing_)



9. Make your program public on GitLab or a web page or forum (_software release_)


10. Profit `;-)` ... or get an A on your assignment



## References

* [`TangibleAI/community/team/exercises/1-text-adventure`](https://gitlab.com/tangibleai/community/team/-/tree/main/exercises/1-text-adventure)
* ["Adventure talk"](https://swenson.github.io/swenson/adventure-talk/) by Chris Swenson at PyCon
* [Python port of Adventure](https://github.com/swenson/adventwure/) by Chris Swenson 
* [Fortran data file](https://github.com/swenson/adventwure/blob/main/advdat.77-03-31.txt) by Willie Crowther and Don Woods
* ["10 Best Text Adventures"](https://lifehacker.com/10-of-the-best-text-based-adventure-games-you-never-kne-1848770935) by Stephen Johnson on LifeHacker
* [online Colossal Cave Adventure fan forum](https://rickadams.org/adventure/) by Rick Adams where you can play Adventure online
