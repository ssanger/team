>>> %run evaluator/measure_accuracy.py
>>> accuracies
{'vish': 0.0,
 'mathtext': 0.40217391304347827,
 'asyrgeldiatayev': 0.06521739130434782,
 'catasaurus': 0.532608695652174}
>>> df
    Unnamed: 0  index                                 input   output      source   format   vish mathtext  asyrgeldiatayev  catasaurus
0            0      0                              fourteen       14    mathtext     yaml  error       14               14          14
1            1      1                               forteen       14    mathtext     yaml  error       14                0          14
2            2      2  one thousand four hundred ninety two     1492    mathtext     yaml  error     1492             1197        1492
3            3      3               one thousand ninety two     1092    mathtext     yaml  error     1092             1093        1092
4            4      4           Fourteen Hundred Ninety-Two     1492    mathtext     yaml  error     1492                0        1492
..         ...    ...                                   ...      ...         ...      ...    ...      ...              ...         ...
87          88      5                                   ten       21    mathtext  doctest  error       10               10          10
88          89      0                 one two three and ten      133  catasaurus  doctest  error       51                0          51
89          90      1      ONE MILLion three hunded and fiv  1000305  catasaurus  doctest  error  1000095                0     1000305
90          91      2                        50,500 and six    50506  catasaurus  doctest  error    error                0       50506
91          92      3                  one_million_and_five  1000005  catasaurus  doctest  error  1000005                0     1000005

[92 rows x 10 columns]
>>> pd.Series(accuracies).sort_values()
vish               0.000000
asyrgeldiatayev    0.065217
mathtext           0.402174
catasaurus         0.532609
dtype: float64
>>> pd.Series(accuracies).sort_values(descending=True)
>>> pd.Series(accuracies).sort_values(ascending=True)
vish               0.000000
asyrgeldiatayev    0.065217
mathtext           0.402174
catasaurus         0.532609
dtype: float64
>>> pd.Series(accuracies).sort_values(ascending=False)
catasaurus         0.532609
mathtext           0.402174
asyrgeldiatayev    0.065217
vish               0.000000
dtype: float64
>>> leader_board = pd.Series(accuracies).sort_values(ascending=False)
>>> leader_board.index = 'catasaurus_v2 catasaurus_v1 asyrgeldiatayev vish'.split()
>>> leader_board
catasaurus_v2      0.532609
catasaurus_v1      0.402174
asyrgeldiatayev    0.065217
vish               0.000000
dtype: float64
>>> hist -o -p -f evaluator/evaluate.ipy.md
