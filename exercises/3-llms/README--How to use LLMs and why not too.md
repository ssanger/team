# How to use LLMs, and why not too

## Bad

1. Marketing and advertisement
2. Social media outreach
3. Job postings
4. Recruiting & application evaluation
5. Internal memos and e-mails
6. Policy and process documentation
7. Documentation
8. Software and business logic
9. Loan and insurance application evaluation
10. Direct-to-consumer fiction (scripts, novels)
11. Crisis hotlines

## Sometimes good

1. Spelling and grammar checking
2. Creative writing prompts
3. Rubber ducking and brainstorming
4. Some mental health (not crisis hotlines)
5. Security and surveillance

## Why not too

### Initial take

1. LLMs might be illegal. Lawsuit filed Nov 4: https://githubcopilotlitigation.com/ ). And they are definitely "unacceptable and unjust" according to #FSF ( https://www.fsf.org/blogs/licensing/fsf-funded-call-for-white-papers-on-philosophical-and-legal-questions-around-copilot ).
1. LLMs are cheap, they generate limitless disposable cheap text, like plastic in the 1970's when it was all the rage
2. LLMs are average, they generate the average content they've read from all the things people have said online in public rather than anything with a unique tone much less personality
3. LLMs generate what the average reader wants to hear... click bait, sarcasm, humor. Facts and common sense don't matter, just how "interesting" it is.
4. LLMs aren't self-aware, they can't explain why they said something or how they feel about it.
5. If you interviewed an LLM as if it were a human would you hire it to do the job you have in mind?  Can it ask the same kinds of "team player", behavioral questions you'd ask a human? Does it have a cohesive personality and understanding of your company, your business, or your customers? Will it be able to learn that? 
6. What about the humans that are currently doing the job? Wouldn't your team, your community, your customers be happier knowing that it was a human they were interacting with rather than a machine?
7. LLMs imitate click bait, because they know that's what human's **Like** on social media.

Give examples for each of these, such as the one the acquisitions editor mentioned

## Another hot take

1. It's illegal in the civilized world (Europe)
2. They're cheap - in a bad way - they will cheapen your personal or business brand
3. They're mindless, thoughtless, sociopathic - they litterally have no care for your existence or wellbeing or anyone else's. They will say whatever their trainers have taught them to say to make you happy (and sing their praises)
4. They're wrong - in subtle ways that you won't notice - they don't know up from down or hot from cold or pain from pleasure - there are limitless examples of illogical and harmful things they will say and do if you don't keep a close eye on them. Would you hire a zombie employee, or therapist, or copyrighter that just copy and pasted pasted whatever it thought best matched your query?
5. It's represents the average of all the garbage knowledge it read on the internet -- mostly advertisement and misinformation and conspiracy theories and posturing. Ask it a syllogism about cigarette's be made of plants and thus being good for you and it will claim they're bad for you... because of the additives... (straight out of the ad campaign for Light and Low Tar cigarettes).
6. They are biased, racist, sexist, anti-semetic and generally sociopathic. Ask it to write a Python function to determine whether someone is a good scientist or not based on their demographics... and you can guess exactly what kind of code it wrote. Or ask it how many humans fit in a cattle car...
7. You are helping make billionaires richer by giving them your data which they will use to take your livelihood and your vote and your brain when you could be instead helping train the prosocial AI bots that compete with them. I prefere to support smart researchers trying to educate teens in 3rd world countries or solve the climate crisis or save people from having to die in the heat of Qatar so we can watch soccer in an air conditioned outdoor stadium. MSN fired all their journalists in 2020 when the first really successful LLMs came online.
8. You are contributing to the downfall of humanity by helping the AI eat its tail (they are trained on the internet and all the garbage that bots spew) GIGO "turtles" all the way down.

## Toots

### #Kaggle alternatives

Looking for #Kaggle alternatives this weekend? #DataScience #Competition

* Codalab https://competitions.codalab.org/competitions/ -  SOTA problems
* Driven data https://www.drivendata.org/competitions/ - "AI for good" climate, ecology, privacy
* DataHack https://datahack.analyticsvidhya.com/
* Zindi https://zindi.africa/competitions African kaggle
* Numer.ai - stock market quant forecasting & trading
* Bitgrit https://bitgrit.net/competition/ -
* #AI crowd https://www.aicrowd.com/ - human ai collaboration in Minecraft ( #NLU )
* more: https://en.m.wikipedia.org/wiki/Competitions_and_prizes_in_artificial_intelligence)


### Mental health support with #GPT3-based #chatbot

Another reason not to use #LLMs for anything important. Rob Morris (RobertRMorris.org) used #GPT-3 as a "co-pilot" for his #MentalHealth peer-support chat platform #Koko. Turns out people like to know when they're chatting with an #authentic #human rather than someone coaxed by AI to express #empathy. 

https://t.co/3gHvc5i0rU


### 3/8 Advent of Large Language Models (#LLM s) 

[Posted 2022-12-2 on Mastodon](https://mastodon.social/@hobs@mstdn.social/10944614216201497)

#3 reason not to use #LLM s is that they are #mindless.

This is not about #consciousness or #SelfAwareness.
LLMs just live in a fantasy world.
Their #mental/crazy model was trained on all the drivel that you and I spew.
Even #Galactica (LLM trained on peer-reviewed journals) will dispassionately explain to you that orally consumed glass has health benefits (see this morning's AI with AI podcast).
You are contributing to the #GIGO<->#GIGO (#Sturgeon's Law) feedback loop if you generate text with an LLM.
Your text will be used to train bigger more mindless LLMs.


## 2/8 Advent of Large Language Models (LLM) (2022-11-10)

#2 reason not to use #LLM s is that they are #cheap.

The accountants and CFOs in the room my think that's a good thing.
Those same cost-conscious folk probably jumped on the bandwagon of polyester too.
But LLMs like #GPT_3 cheapen your brand and your connection to your #community.

Your words are your voice, your thoughts, the mind of your community.
Make every one count.

If a bot can convey what you are thinking better than you can, that should give you pause.
Did you decide to post that catchy inauthentic meme, or have you ceded your #agency and #creativity to an #algorithm and the #Miasma (#dystopian #Internet) mob?

Stay tuned for 6 more reasons to think twice about using #LLMs.

@mdyshel 
#BigTech #PaLM #T5 #Megatron #BERT-Large

## 1/8 reasons not to use Large Language Models (2022-11-04)

#1 reason not to use #LLM s is that they might be illegal (lawsuit filed today: https://githubcopilotlitigation.com/ ) and they are definitely "unacceptable and unjust" according to #FSF ( https://www.fsf.org/blogs/licensing/fsf-funded-call-for-white-papers-on-philosophical-and-legal-questions-around-copilot )

I'll post one reason a week to round out this new year of big tech #AI "innovation" @mdyshel 
#openai #GPT_3 #microsoft #GitHub #copilot #google #deepmind #PaLM #T5 #Megatron #BERT-Large)

## 1/8 Large Language Models may harm your business 

If you value your customers, business, or community think twice before using  #LLM's (#GPT_3 #PaLM #Copilot) in your organization.

A senior executive told me how lately most meetings end with someone saying "We should try that with GPT-3." He asked me what I thought. I gave him a **LOT** to think about.

Each week @mdyshel and I will toot one thought about the harm of LLMs on your business, with examples. 

Share yours please.
