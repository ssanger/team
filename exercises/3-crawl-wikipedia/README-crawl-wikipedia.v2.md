# Crawl Wikipedia

There are at least 4 ways to download data from Wikipedia:

1. Directly scrape the HTML with the `requests` package using [this GeeksForGeeks tutorial](https://www.geeksforgeeks.org/web-scraping-from-wikipedia-using-python-a-complete-guide/)
2. Use `requests` to query [the Wikipedia API](https://www.jcchouinard.com/wikipedia-api/)
3. Use the [`Wikipedia`](https://gitlab.com/tangibleai/Wikipedia) package to download preprocessed text
4. Use the [`Wikipedia-API` package](https://pypi.org/project/Wikipedia-API/) to download preprocessed text

## Resources

- Tangible AI's [fork of the `Wikipedia`](https://gitlab.com/tangibleai/Wikipedia) source code
- [PyPi Wikipedia-API package](https://pypi.org/project/Wikipedia-API/)
- Unmaintained [PyPi Wikipedia package](https://pypi.org/project/Wikipedia/)
- tutorial on [using python to access the web api](https://www.jcchouinard.com/wikipedia-api/)
